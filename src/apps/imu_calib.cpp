#include <iostream>

#include "imu_tk/io_utils.h"
#include "imu_tk/calibration.h"
#include "imu_tk/filters.h"
#include "imu_tk/integration.h"
#include "imu_tk/visualization.h"

#include <cstdint>
#include "csv_parser/csv.h"

using namespace std;
using namespace imu_tk;
using namespace Eigen;

int main(int argc, char** argv)
{
  // if( argc < 3 )
  //   return -1;

  vector< TriadData > acc_data, gyro_data;

  int64_t time_stamp; 
  double gyro[3]; 
  double accel[3];
  // io::CSVReader<7> in("/home/rdu/Workspace/imu_calib/data/raw_imu.20171111235018.data");
  // io::CSVReader<7> in("/home/rdu/Workspace/imu_calib/data/raw_imu.20171122145342.data");
  io::CSVReader<7> in("/home/rdu/Workspace/imu_calib/data/raw_imu.20171122151921.data");
    
  acc_data.clear();
  gyro_data.clear();
  while(in.read_row(time_stamp, accel[0], accel[1], accel[2], gyro[0], gyro[1], gyro[2])){
    // std::cout << "data: " << time_stamp << " , " << gyro[0] << " , " << gyro[1] << " , " << gyro[2] << " , "
    //         << accel[0] << " , " << accel[1] << " , " << accel[2] << std::endl;

        double ts = time_stamp/1000.0;
        acc_data.push_back ( TriadData(ts, accel[0], accel[1], accel[2]) );
        gyro_data.push_back ( TriadData(ts, gyro[0], gyro[1], gyro[2]) );
  } 
  
  CalibratedTriad init_acc_calib, init_gyro_calib;
  init_acc_calib.setBias( Vector3d(.01, .01, .01) );
  init_gyro_calib.setScale( Vector3d(1.0/6258.0, 1.0/6258.0, 1.0/6258.0) );
  
  MultiPosCalibration mp_calib;
    
  mp_calib.setInitStaticIntervalDuration(50.0);
  mp_calib.setInitAccCalibration( init_acc_calib );
  mp_calib.setInitGyroCalibration( init_gyro_calib );  
  mp_calib.setGravityMagnitude(9.80503);
  mp_calib.enableVerboseOutput(true);
  mp_calib.enableAccUseMeans(false);
  mp_calib.setGyroDataPeriod(1.0/300.0);
  
  mp_calib.calibrateAccGyro(acc_data, gyro_data );
  mp_calib.getAccCalib().save("test_imu_acc.calib");
  mp_calib.getGyroCalib().save("test_imu_gyro.calib");
  
//   for( int i = 0; i < acc_data.size(); i++)
//   {
//     cout<<acc_data[i].timestamp()<<" "
  //         <<acc_data[i].x()<<" "<<acc_data[i].y()<<" "<<acc_data[i].z()<<" "
  //         <<gyro_data[i].x()<<" "<<gyro_data[i].y()<<" "<<gyro_data[i].z()<<endl;
//   }
//   cout<<"Read "<<acc_data.size()<<" tuples"<<endl;
  
  return 0;
}